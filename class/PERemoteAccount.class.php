<?php


require_once(__DIR__ . "/../../pe-graphql/vendor/autoload.php");

use GraphQL\Error\ClientAware;
use GraphQL\Utils\BuildSchema;
use GraphQL\Utils\SchemaPrinter;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;



class PERemoteAccount extends SMC_Post
{
	static $test_edit;
	static function get_type()
	{
		return PE_REMOTE_ACCOUNT_TYPE;
	}
	static function init()
	{
		add_action('init', 						[ __CLASS__, 'register_all' ], 18);
		
		parent::init();
	}
	
	static function register_all()
	{
		$labels = array(
			'name'               => __("Remoute Account", PESRV), // Основное название типа записи
			'singular_name'      => __("Remoute Account", PESRV), // отдельное название записи типа Book
			'add_new'            => __("add Remoute Account", PESRV), 
			'all_items' 		 => __('Remoute Accounts', PESRV),
			'add_new_item'       => __("add Remoute Account", PESRV), 
			'edit_item'          => __("edit Remoute Account", PESRV), 
			'new_item'           => __("add Remoute Account", PESRV), 
			'view_item'          => __("see Remoute Account", PESRV), 
			'search_items'       => __("search Remoute Account", PESRV), 
			'not_found'          => __("no Remoute Accounts", PESRV), 
			'not_found_in_trash' => __("no Remoute Accounts in trash", PESRV), 
			'parent_item_colon'  => '',
			'menu_name'          => __("Remoute Accounts", PESRV), 
		);
		register_post_type(
			PE_REMOTE_ACCOUNT_TYPE, 
			[
				'labels'             => $labels,
				'public'             => true,
				'publicly_queryable' => true,
				'show_ui'            => true,
				'show_in_menu'       => 'pesrv_page',
				'query_var'          => true,
				'capability_type'    => 'post',
				'has_archive'        => true,
				'hierarchical'       => false,
				'menu_position'      => 7,
				"menu_icon"			 => "dashicons-megaphone",
				'supports'           => array('title','editor', 'author'),
				"rewrite"			 => ["slug" => ""]
			]
		);
	}
}
